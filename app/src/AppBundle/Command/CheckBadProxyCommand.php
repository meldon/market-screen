<?php

namespace AppBundle\Command;

use AppBundle\ConnectProcessor\ConnectProcessor;
use AppBundle\Entity\Proxy;
use AppBundle\Repository\ProxyRepository;
use Spatie\Browsershot\Browsershot;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CheckBadProxyCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('check-bad-proxy')
            ->setDescription('...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**@var ProxyRepository $proxyRep */
        $proxyRep = $this->getContainer()->get('doctrine')->getRepository("AppBundle:Proxy");
        $em = $this->getContainer()->get('doctrine')->getManager();
        $proxys = $proxyRep->findBy(['status'=>Proxy::STATUS_IS_NOT_ALIVE]);
        $flushNumber = 0;
        var_dump('NOT ALIVE');
        var_dump(count($proxys));
        /**@var Proxy $proxy */
        foreach ($proxys as $proxy){
            $result = ConnectProcessor::isProxyAlive($proxy->getProxyString(),$proxy->getIsSocks5());
            if($result){
                $proxy->setStatus(Proxy::STATUS_FREE);
                $em->merge($proxy);
                $flushNumber++;
            }
            if($flushNumber>10){
                $flushNumber=0;
                $em->flush();
            }
        }
        $em->flush();
        $proxys = $proxyRep->findBy(['status'=>Proxy::STATUS_BAN]);

        var_dump('BAN');
        var_dump(count($proxys));
        $flushNumber = 0;
        /**@var Proxy $proxy */
        foreach ($proxys as $proxy){
            if($flushNumber>10){
                $flushNumber=0;
                $em->flush();
            }

            $result = ConnectProcessor::isProxyAlive($proxy->getProxyString(),$proxy->getIsSocks5());
            if(!$result) {
                $proxy->setStatus(Proxy::STATUS_IS_NOT_ALIVE);
                $em->merge($proxy);
                $flushNumber++;
                continue;
            }

            $options = ConnectProcessor::getOptions('https://market.yandex.by/',$proxy->getProxyString(),$proxy->getIsSocks5());
            $html = ConnectProcessor::getHtml($options);
            if(!ConnectProcessor::checkHtmlIsBan($html)){
                $proxy->setStatus(Proxy::STATUS_FREE);
                $em->merge($proxy);
                $flushNumber++;
                continue;
            }
        }
        $em->flush();
    }

}
