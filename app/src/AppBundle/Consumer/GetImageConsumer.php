<?php

namespace AppBundle\Consumer;
use AppBundle\ConnectProcessor\ConnectProcessor;
use AppBundle\Entity\Proxy;
use AppBundle\Entity\Queue;
use Doctrine\Bundle\DoctrineBundle\Registry;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use Spatie\Browsershot\Browsershot;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class GetImageConsumer  implements ConsumerInterface
{

    private $doctrine;

    public function __construct( Registry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function execute(\PhpAmqpLib\Message\AMQPMessage $msg)
    {
        $id = $msg->getBody();
        var_dump($id);
        $queue = $this->doctrine->getRepository('AppBundle:Queue')->find($id);
        if(is_null($queue)){
            return;
        }
        $path = $this->getImage($queue->getUrl());
        var_dump($path);
        if(is_array($path)){
            $queue->setErrorData(json_encode($path));
            $queue->setLastDateUpdate(new \DateTime('now'));
            $this->doctrine->getManager()->merge($queue);
            $this->doctrine->getManager()->flush();
            return;
        }
        $queue->setImagePath($path);
        $queue->setStatus(Queue::STATUS_PASS);
        $queue->setLastDateUpdate(new \DateTime('now'));
        $this->doctrine->getManager()->merge($queue);
        $this->doctrine->getManager()->flush();
    }

    public function getImage($url){
        $proxyRep = $this->doctrine->getRepository("AppBundle:Proxy");
        $em =$this->doctrine->getManager();
        /**@var Proxy $proxy */
        $proxy = $proxyRep->getFreeProxy();
        if (is_null($proxy)) {
            return [
                'status' => 104,
                'message' => 'No free or working proxies found'
            ];
        }
        $options = ConnectProcessor::getOptions($url, $proxy->getProxyString(), $proxy->getIsSocks5());
        $x = 5;
        $html = null;
        while ($x != 0) {
            $html = ConnectProcessor::getHtml($options);
            if (is_array($html)) {
                $x--;
                sleep(1);
                continue;
            }
            if(ConnectProcessor::checkHtmlIsBan($html)){
                $proxy->setStatus(Proxy::STATUS_BAN);
                $em->merge($proxy);
                $em->flush();
                $html = [
                    'status'=>104,
                    'message'=>'No free or working proxies found'
                ];
                continue;
            }
            break;
        }
        if (is_array($html)) {
            return $html;
        }

        $html = str_replace("<!DOCTYPE html>", "<!DOCTYPE html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />", $html);
        $fileBufPath = __DIR__ . "/../../../web/";
        $fileImagePath = __DIR__ . "/../../../web/img/";
        $htmlBufName = uniqid('html_') . ".html";
        $imageBufName = uniqid('img_') . ".jpeg";
        file_put_contents($fileBufPath . $htmlBufName, $html);
        Browsershot::url("http://nginx/" . $htmlBufName)
            ->fullPage()
            ->setScreenshotType('jpeg', 100)
            ->noSandbox()
            ->save($fileImagePath . $imageBufName);
        unlink($fileBufPath . $htmlBufName);
        return 'img/'.$imageBufName;
    }

}