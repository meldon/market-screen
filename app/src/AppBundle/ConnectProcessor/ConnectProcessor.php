<?php
/**
 * Created by PhpStorm.
 * User: meldon
 * Date: 19.02.18
 * Time: 11:38
 */

namespace AppBundle\ConnectProcessor;


class ConnectProcessor
{
    const DEFAULT_OPTION = array(
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_HEADER => false,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_ENCODING => "",
        CURLOPT_USERAGENT => "spider",
        CURLOPT_AUTOREFERER => true,
        CURLOPT_CONNECTTIMEOUT => 10,
        CURLOPT_TIMEOUT => 50,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_SSL_VERIFYPEER => false
    );

    /**
     * @param string $html
     * @return \DOMDocument
     */
    public static function getDocument($html)
    {
        $document = new \DOMDocument();
        libxml_use_internal_errors(true);
        $document->loadHTML($html);
        return $document;
    }

    public static function checkHtmlIsBan($html){
        $elements = self::findByXpath($html,'.//*[contains(@class,"form__captcha")');

        if(!$elements){
            return false;
        }
        return true;
    }

    /**
     * @param $options
     * @return mixed|null
     */
    public static function getHtml($options)
    {
        $ch = null;
        $data = null;
        while ($ch == null) {
            $ch = curl_init();
        }
        curl_setopt_array($ch, $options);
        $data = curl_exec($ch);
        if (!$data) {
            return [
                'status'=>102,
                'message'=>'Error curl execute. Details: '.curl_error($ch),
            ];
        }

        curl_close($ch);
        libxml_use_internal_errors(true);
        return $data;
    }

    public static function getCookie($result)
    {
        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);

        $cookie = "";
        foreach ($matches[1] as $match) {
            $cookie .= $match . '; ';
        }
        return trim($cookie);
    }

    public static function getOptions($url, $proxy = null, $isSocks5 = true)
    {
        $options = self::DEFAULT_OPTION;
        $options[CURLOPT_URL] = $url;
        if (!is_null($proxy)) {
            $prefix = "";
            if ($isSocks5 == true) {
                $prefix = "socks5://";
            }
            $proxyString = $prefix . $proxy;
            $options[CURLOPT_PROXY] = $proxyString;
        }
        return $options;
    }


    public static function getPostOptions($url, $refererUrl, $cookie)
    {
        $options = self::DEFAULT_OPTION;
        $options[CURLOPT_POST] = false;
        $options[CURLOPT_REFERER] = $refererUrl;
        $options[CURLOPT_URL] = $url;
        $options[CURLOPT_COOKIE] = $cookie;
        $options[CURLOPT_HEADER] = false;
        return $options;
    }

    public static function findByXpath($html, $xpath)
    {
        $doc = self::getDocument($html);
        $domXpath = new \DOMXpath($doc);
        return $domXpath->query($xpath);
    }

    public static function isProxyAlive($proxy, $isSocks5)
    {
        $prefix="";
        if ($isSocks5 == true) {
            $prefix = "socks5://";
        }
        $proxy = $prefix .$proxy;
        $url = 'https://www.google.com/';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HEADER, true);
        $curl_scraped_page = curl_exec($ch);
        return (boolean)$curl_scraped_page;
    }


}