<?php

namespace AppBundle\Controller;

use AppBundle\ConnectProcessor\ConnectProcessor;
use AppBundle\Entity\Proxy;
use AppBundle\Entity\Queue;
use AppBundle\Repository\ProxyRepository;
use Spatie\Browsershot\Browsershot;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;


class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     * @Method({"GET", "POST"})
     */
    public function indexAction(Request $request)
    {
        $proxy = new Proxy();
        $form = $this->createForm('AppBundle\Form\ProxyType', $proxy);
        $form->handleRequest($request);
        $countInProgress = $this->getDoctrine()->getRepository('AppBundle:Queue')->createQueryBuilder('x')->select('count(x.id)')->where("x.status='".Queue::STATUS_IN_PROGRESS."'")->getQuery()->getScalarResult();
        $countPass = $this->getDoctrine()->getRepository('AppBundle:Queue')->createQueryBuilder('x')->select('count(x.id)')->where("x.status='".Queue::STATUS_PASS."'")->getQuery()->getScalarResult();

        if ($form->isSubmitted()) {
            $proxys = explode(PHP_EOL, trim($proxy->getProxyString()));
            $dateCreate = new \DateTime('now');
            $em = $this->getDoctrine()->getManager();
            foreach ($proxys as $proxyString) {
                $proxyEntity = new Proxy();
                $proxyEntity->setStatus(Proxy::STATUS_FREE);
                $proxyEntity->setDataCreate($dateCreate);
                $proxyEntity->setLastTimeConnections($dateCreate);
                $proxyEntity->setProxyString($proxyString);
                $proxyEntity->setIsSocks5($proxy->getIsSocks5());
                $em->persist($proxyEntity);
            }
            $this->addFlash('success', 'Было добавлено ' . count($proxys) . ' прокси');
            $em->flush();
        }
        $proxysEntities = $this->getDoctrine()->getRepository('AppBundle:Proxy')->findBy([], ['id' => 'DESC']);
        var_dump($countPass);
        return $this->render('default/index.html.twig', array(
            'proxy' => $proxy,
            'proxys' => $proxysEntities,
            'form' => $form->createView(),
            'queue_stats' => ['pass'=>$countPass[0][1],'in_progress'=>$countInProgress[0][1]]
        ));
    }


    /**
     * @Route("/get_screen", name="getscreen")
     */
    public function getScreenAction(Request $request)
    {
        $url = $request->query->get('url');
        if (is_null($url)) {
            $response = new Response(json_encode(array(
                'status' => 101,
                'message' => 'Missing or invalid parameter keys'
            )));
            return $response;
        }
        $image = $this->getImage($url);
        if (is_array($image)) {
            $response = new Response(json_encode($image));
            return $response;
        }
        $response = new Response(json_encode(
            [
                'status' => 200,
                'message' => 'OK',
                'image' => $image
            ]
        ));
        return $response;
    }


    /**
     * @Route("/view_get_screen", name="viewgetscreen")
     */
    public function viewGetScreenAction(Request $request)
    {
        $url = $request->query->get('url');
        $data = [];
        if (is_null($url)) {
            return $this->render('default/view.html.twig', ['data'=>['status' => 101,
                'message' => 'Missing or invalid parameter keys']]);
        }
        $image = $this->getImage($url);
        if (is_array($image)) {
            return $this->render('default/view.html.twig', ['data' => $image]);
        }

        $data = ['status' => 200, 'message' => 'OK', 'image' => $image];
        return $this->render('default/view.html.twig', ['data' => $data]);
    }

    private function getImage($url)
    {
        /**@var ProxyRepository $proxyRep */
        $proxyRep = $this->get('doctrine')->getRepository("AppBundle:Proxy");
        $em = $this->get('doctrine')->getManager();
        /**@var Proxy $proxy */
        $proxy = $proxyRep->getFreeProxy();
        if (is_null($proxy)) {
            return [
                'status' => 104,
                'message' => 'No free or working proxies found'
            ];
        }
        $options = ConnectProcessor::getOptions($url, $proxy->getProxyString(), $proxy->getIsSocks5());
        $x = 5;
        while ($x != 0) {
            $html = ConnectProcessor::getHtml($options);
            if (is_array($html)) {
                $x--;
                sleep(1);
                continue;
            }
            if(ConnectProcessor::checkHtmlIsBan($html)){
                $proxy->setStatus(Proxy::STATUS_BAN);
                $em->merge($proxy);
                $em->flush();
                $html = [
                    'status'=>104,
                    'message'=>'No free or working proxies found'
                ];
                continue;
            }
            break;
        }
        if (is_array($html)) {
            return $html;
        }

        $html = str_replace("<!DOCTYPE html>", "<!DOCTYPE html><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />", $html);
        $fileBufPath = __DIR__ . "/../../../web/";
        $fileImagePath = __DIR__ . "/../../../web/img/";
        $htmlBufName = uniqid('html_') . ".html";
        $imageBufName = uniqid('img_') . ".jpeg";
        file_put_contents($fileBufPath . $htmlBufName, $html);
        Browsershot::url("http://nginx/" . $htmlBufName)
            ->fullPage()
            ->setScreenshotType('jpeg', 100)
            ->noSandbox()
            ->save($fileImagePath . $imageBufName);
        unlink($fileBufPath . $htmlBufName);
        return 'img/' . $imageBufName;
    }

    /**
     * @Route("/create_queue", name="createqueue",methods={"POST"})
     */
    public function makeQueueAction(Request $request)
    {
        $url = $request->request->get('url');
        if (is_null($url)) {
            $response = new Response(json_encode(array(
                'status' => 101,
                'message' => 'Missing or invalid parameter keys'
            )));
            return $response;
        }

        $queue = new Queue();
        $queue->setUrl($url);
        $queue = $this->getDoctrine()->getManager()->merge($queue);
        $this->getDoctrine()->getManager()->flush();
        $response = new Response(json_encode(
            [
                'status' => 200,
                'message' => 'OK',
                'queue_id' => $queue->getId()
            ]
        ));
        return $response;
    }


    /**
     * @Route("/get_queue/{id}", name="getqueue")
     */
    public function getQueueAction(Request $request,Queue $queue)
    {
        if($queue->getStatus()==Queue::STATUS_PASS){
            $response = new Response(json_encode(
                [
                    'status' => 200,
                    'message' => 'OK',
                    'image_path' => $queue->getImagePath()
                ]
            ));
            return $response;
        }else{
            $response = new Response(json_encode(
                [
                    'status' => 199,
                    'message' => 'queue in work...'
                ]
            ));
            return $response;
        }
    }

    }