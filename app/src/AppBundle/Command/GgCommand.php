<?php

namespace AppBundle\Command;

use AppBundle\ConnectProcessor\ConnectProcessor;
use AppBundle\Entity\Proxy;
use AppBundle\Repository\ProxyRepository;
use Spatie\Browsershot\Browsershot;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GgCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('gg')
            ->setDescription('...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

    }

}
