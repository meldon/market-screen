<?php

namespace AppBundle\Command;

use AppBundle\ConnectProcessor\ConnectProcessor;
use AppBundle\Entity\Proxy;
use AppBundle\Entity\Queue;
use AppBundle\Repository\ProxyRepository;
use Spatie\Browsershot\Browsershot;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendRabbitCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('send-rabbit')
            ->setDescription('...');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        while (true) {
            if (!$this->checkQueueRabbitMQ()) {
                $queues = $this->getContainer()->get('doctrine')->getRepository('AppBundle:Queue')->findBy(['status' => Queue::STATUS_IN_PROGRESS], [], 100);
                foreach ($queues as $queue) {
                    $this->send($queue->getId());
                }
                var_dump('SEND '.count($queues).' MESSAGES');
            }
            sleep(10);
        }

    }


    //Проверяем есть ли в очереди задачи
    private function checkQueueRabbitMQ()
    {
        /** @var \OldSound\RabbitMqBundle\RabbitMq\Producer $rabbit */
        $rabbit = $this->getContainer()->get('old_sound_rabbit_mq.get_image_consumer_producer');
        $result = $rabbit->getChannel()->queue_declare('get_image_consumer', false, true, false, false);

        if ($result[1]) {
            return true;
        }
        return false;
    }

    public function send($id)
    {
        /** @var \OldSound\RabbitMqBundle\RabbitMq\Producer $rabbit */
        $rabbit = $this->getContainer()->get('old_sound_rabbit_mq.get_image_consumer_producer');
        $rabbit->publish($id);
    }

}
