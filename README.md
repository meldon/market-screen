**Как развернуть**
1. Установить docker https://docs.docker.com/install/linux/docker-ce/ubuntu/
2. Установить docker-compose https://docs.docker.com/compose/install/
3. зайти в docker/nginx/conf.d и сделать дубликат файла default.dev/prod с расширением .conf (default.conf)
4. Залить исходники в любую папку
5. Зайти в папку с проектом
6. docker-compose up -d
7. docker-compose exec php7 composer install  --ignore-platform-reqs
7. docker-compose exec php7 bin/console do:d:cre
9. docker-compose exec php7 bin/console do:s:cre
10. docker-compose exec php7 chmod -R 0777 var/
11. docker-compose exec php7 bin/console fos:user:create


**Api**

(POST) /create_queue [url - https://market.yandex.ru/...] - создать очередь для создания скриншота. Пример ответа: {"status":200,"message":"OK","queue_id":7}

 
(GET/get_queue/{queue_id}  - получение результата выполнения задачи. Примеры ответа:
{"status":200,"message":"OK","image_path":"img/img_5b843d3b8c69f.jpeg"} - задание выполнено
{"status":199,"message":"queue in work..."} - задание в работе
   