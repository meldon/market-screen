<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Proxy
 *
 * @ORM\Table(name="proxy")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProxyRepository")
 */
class Proxy
{
    const STATUS_FREE = 'FREE';
    const STATUS_IS_NOT_ALIVE = 'IS_NOT_ALIVE';
    const STATUS_BAN = 'BAN';
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dataCreate", type="datetime")
     */
    private $dataCreate;

    /**
     * @var string
     *
     * @ORM\Column(name="proxy_string", type="string", length=255)
     */
    private $proxyString;

    /**
     * @var bool
     *
     * @ORM\Column(name="isSocks5", type="boolean")
     */
    private $isSocks5;




    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_time_connections", type="datetime")
     */
    private $lastTimeConnections;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDataCreate()
    {
        return $this->dataCreate;
    }

    /**
     * @param \DateTime $dataCreate
     */
    public function setDataCreate($dataCreate)
    {
        $this->dataCreate = $dataCreate;
    }

    /**
     * @return string
     */
    public function getProxyString()
    {
        return $this->proxyString;
    }

    /**
     * @param string $proxyString
     */
    public function setProxyString($proxyString)
    {
        $this->proxyString = $proxyString;
    }

    /**
     * @return bool
     */
    public function getIsSocks5()
    {
        return $this->isSocks5;
    }

    /**
     * @param bool $isSocks5
     */
    public function setIsSocks5($isSocks5)
    {
        $this->isSocks5 = $isSocks5;
    }

    /**
     * @return \DateTime
     */
    public function getLastTimeConnections()
    {
        return $this->lastTimeConnections;
    }

    /**
     * @param \DateTime $lastTimeConnections
     */
    public function setLastTimeConnections($lastTimeConnections)
    {
        $this->lastTimeConnections = $lastTimeConnections;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }


}
