<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Queue
 *
 * @ORM\Table(name="queue")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QueueRepository")
 */
class Queue
{
    const STATUS_IN_PROGRESS = 'IN_PROGRESS';
    const STATUS_PASS = 'PASS';


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="text")
     */
    private $url;

    /**
     * @var string
     *
     * @ORM\Column(name="image_path", type="text",nullable=true)
     */
    private $imagePath;

    /**
     * @var string
     *
     * @ORM\Column(name="error_data", type="text",nullable=true)
     */
    private $errorData;

    /**
     * @var string
     *
     * @ORM\Column(name="date_create", type="datetime")
     */
    private $dateCreate;

    /**
     * @var string
     *
     * @ORM\Column(name="last_date_update", type="datetime", nullable=true)
     */
    private $lastDateUpdate;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=255)
     */
    private $status;

    /**
     * Queue constructor.
     */
    public function __construct()
    {
        $this->status = Queue::STATUS_IN_PROGRESS;
        $this->dateCreate = new \DateTime('now');
    }


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set url.
     *
     * @param string $url
     *
     * @return Queue
     */
    public function setUrl($url)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set imagePath.
     *
     * @param string $imagePath
     *
     * @return Queue
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath.
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return Queue
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return string
     */
    public function getErrorData()
    {
        return $this->errorData;
    }

    /**
     * @param string $errorData
     */
    public function setErrorData($errorData)
    {
        $this->errorData = $errorData;
    }

    /**
     * @return string
     */
    public function getDateCreate()
    {
        return $this->dateCreate;
    }

    /**
     * @param string $dateCreate
     */
    public function setDateCreate($dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    /**
     * @return string
     */
    public function getLastDateUpdate()
    {
        return $this->lastDateUpdate;
    }

    /**
     * @param string $lastDateUpdate
     */
    public function setLastDateUpdate($lastDateUpdate)
    {
        $this->lastDateUpdate = $lastDateUpdate;
    }


}
