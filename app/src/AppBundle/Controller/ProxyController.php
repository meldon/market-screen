<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Proxy;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Proxy controller.
 *
 * @Route("proxy")
 */
class ProxyController extends Controller
{
    /**
     * Lists all proxy entities.
     *
     * @Route("/", name="proxy_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $proxies = $em->getRepository('AppBundle:Proxy')->findAll();

        return $this->render('proxy/index.html.twig', array(
            'proxies' => $proxies,
        ));
    }

    /**
     * Creates a new proxy entity.
     *
     * @Route("/new", name="proxy_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $proxy = new Proxy();
        $form = $this->createForm('AppBundle\Form\ProxyType', $proxy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($proxy);
            $em->flush();

            return $this->redirectToRoute('proxy_show', array('id' => $proxy->getId()));
        }

        return $this->render('proxy/new.html.twig', array(
            'proxy' => $proxy,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a proxy entity.
     *
     * @Route("/{id}", name="proxy_show")
     * @Method("GET")
     */
    public function showAction(Proxy $proxy)
    {
        $deleteForm = $this->createDeleteForm($proxy);

        return $this->render('proxy/show.html.twig', array(
            'proxy' => $proxy,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing proxy entity.
     *
     * @Route("/{id}/edit", name="proxy_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Proxy $proxy)
    {
        $deleteForm = $this->createDeleteForm($proxy);
        $editForm = $this->createForm('AppBundle\Form\ProxyType', $proxy);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('proxy_edit', array('id' => $proxy->getId()));
        }

        return $this->render('proxy/edit.html.twig', array(
            'proxy' => $proxy,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a proxy entity.
     *
     * @Route("/{id}", name="proxy_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Proxy $proxy)
    {
        $form = $this->createDeleteForm($proxy);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($proxy);
            $em->flush();
        }

        return $this->redirectToRoute('proxy_index');
    }

    /**
     * Creates a form to delete a proxy entity.
     *
     * @param Proxy $proxy The proxy entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Proxy $proxy)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('proxy_delete', array('id' => $proxy->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
